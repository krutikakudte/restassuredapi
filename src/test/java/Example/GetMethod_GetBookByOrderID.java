package Example;

import io.restassured.RestAssured;

public class GetMethod_GetBookByOrderID {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		RestAssured
		.given()
			.log()
			.all()
			.baseUri("https://simple-books-api.glitch.me/")
			.basePath("orders/{OderId}")
			.pathParam("OderId", "vFHr2DW13ocSmueftZXad")
			.header("Content-Type","application/json")
			.header("Authorization","Bearer d00657c08343aa1709fdbccc2306385a2a0404e19f1cad56bc2d092b63de7755")
		.when()
			.get()
		.then()
			.log()
			.all();
	

	}

}
