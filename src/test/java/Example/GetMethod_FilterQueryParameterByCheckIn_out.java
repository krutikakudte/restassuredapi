/**
 * 
 */
package Example;

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;

/**
 * @author krutika.k
 *
 */
public class GetMethod_FilterQueryParameterByCheckIn_out {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Map<String,Object> BasePath = new HashMap<String,Object>();
		BasePath.put("BaseURLValue", "booking");

		Map<String,Object> QueryParameters = new HashMap<String,Object>();
		QueryParameters.put("CheckIn", "2014-03-13");
		QueryParameters.put("CheckOut", "2014-05-21");
		
		RestAssured
		.given()
			.log()
			.all()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("{BaseURLValue}?{CheckIn}?{CheckOut}")
			.pathParams(BasePath)
			.queryParams(QueryParameters)
			
		.when()
			.get()
		.then()
			.statusCode(200)
			.log()
			.all();
		
	}

}
