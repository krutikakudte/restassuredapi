package Example;

import io.restassured.RestAssured;

public class PutRequest_UpdateExample {

	public static void main(String[] args) 
	{
		RestAssured
			.given()
				.log()
				.all()
				.baseUri("https://simple-books-api.glitch.me/")
				.basePath("orders/{OrderID}")
				.pathParam("OrderID", "vFHr2DW13ocSmueftZXad")
				.body("{\r\n"
						+ "    \"customerName\": \"Shashank Honnalli123\"\r\n"
						+ "}")
				.header("Content-Type","application/json")
				.header("Authorization","Bearer d00657c08343aa1709fdbccc2306385a2a0404e19f1cad56bc2d092b63de7755")
			.when()
				.patch()
			.then()
				.log()
				.all();

	}

}
