package Example;

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;

public class GetMethod_FilterBookByNames
{

	public static void main(String[] args) 
	{
		Map<String,Object> BaseParametre = new HashMap<String,Object>();
		BaseParametre.put("BasePath", "booking");
		
		Map<String,Object> QueryParametres = new HashMap<String,Object>();
		QueryParametres.put("FirstName", "sally");
		QueryParametres.put("LastName", "brown");
		
		
		
		RestAssured
			.given()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("{BasePath}?{FirstName}?{LastName}")
			.pathParams(BaseParametre)
			.queryParams(QueryParametres)
		.when()
			.get()
			.then()
			.log()
			.status();

	}

}
