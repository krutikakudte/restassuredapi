package Example;



import io.restassured.RestAssured;

public class BearerTokenAutorizationfor_BookOrder {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
//		Map<String,Object> BasePath = new HashMap<String,Object>();
//		BasePath.put("BasePath", "orders");
		
		RestAssured
			.given()
				.log()
				.all()
				.baseUri("https://simple-books-api.glitch.me/")
				.basePath("orders")
				.body("{\r\n"
					+ "    \"bookId\": 1,\r\n"
					+ "    \"customerName\": \"Shashank3\"\r\n"
					+ "}")
				.header("Content-Type","application/json")
				.header("Authorization","Bearer d00657c08343aa1709fdbccc2306385a2a0404e19f1cad56bc2d092b63de7755")
			.when()
				.post()
			.then()
				.statusCode(201)
				.log()
				.all();
			
		
		

	}

}
