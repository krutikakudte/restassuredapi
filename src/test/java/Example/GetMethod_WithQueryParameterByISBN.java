package Example;

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;

public class GetMethod_WithQueryParameterByISBN {

	public static void main(String[] args) 
	{
		Map<String,Object> BaseParameters = new HashMap<String,Object>();
		BaseParameters.put("BasePath","Book");
		
		Map<String,Object> QueryParameters = new HashMap<String,Object>();
		QueryParameters.put("ISBN","9781449325862");
		
		RestAssured
			.given()
			.log()
			.all()
			.baseUri("https://bookstore.toolsqa.com/BookStore/v1/")
			.basePath("{BasePath}?{ISBN}")
			.pathParams(BaseParameters)
			.queryParams(QueryParameters)
			
//			.queryParam("ISBN", "9781449325862")
//			.basePath("{BasePath}?{ISBN}")
//			.pathParam("BasePath", "Book")
//			.queryParam("ISBN","9781449325862")
			

		.when()
			.get()
		.then()
			.log()
			.all();
		

	}

}
