package Example;

import io.restassured.RestAssured;

public class DeleteRequest_Example {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		RestAssured
			.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/")
				.basePath("booking/11")
				.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
			.when()
				.delete()
			.then()
				.assertThat()
				.statusCode(201)
				.log()
				.all();

	}

}
