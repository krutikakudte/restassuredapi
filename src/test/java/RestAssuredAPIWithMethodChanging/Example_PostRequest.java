package RestAssuredAPIWithMethodChanging;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class Example_PostRequest {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		RequestSpecification RestAssuredObject=RestAssured.given();
		RestAssuredObject.baseUri("https://bookstore.toolsqa.com/Account/v1/");
		RestAssuredObject.basePath("User");
		RestAssuredObject.body("{\r\n"
				+ "  \"userName\": \"Krutika345777\",\r\n"
				+ "  \"password\": \"Shashank.Honnalli@123\"\r\n"
				+ "}");
		RestAssuredObject.contentType("application/json");
		Response ResponseValue = RestAssuredObject.post();
		ResponseValue.then().log().all();
		
		//Validation Response
		ValidatableResponse Validate = ResponseValue.then();
		Validate.statusCode(201);
		//Validate.log().status();
		
	}

}
