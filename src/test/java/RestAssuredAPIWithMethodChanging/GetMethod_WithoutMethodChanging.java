package RestAssuredAPIWithMethodChanging;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class GetMethod_WithoutMethodChanging {

	public static void main(String[] args)
	{
		//Build Request
		RequestSpecification RequestValue = RestAssured.given();
		RequestValue.baseUri("https://restful-booker.herokuapp.com/");
		RequestValue.basePath("booking/{id}");
		RequestValue.pathParam("id", 19);
		
		
		//Hit request an dget response
		Response ResponseValue = RequestValue.get();
		ValidatableResponse ValidateValue = ResponseValue.then();
		ValidateValue.statusCode(200);
		ValidateValue.log().all();
		
		
		
		
		
				
	}

}
