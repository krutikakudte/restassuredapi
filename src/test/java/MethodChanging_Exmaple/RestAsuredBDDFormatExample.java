package MethodChanging_Exmaple;

import io.restassured.RestAssured;

public class RestAsuredBDDFormatExample {

	public static void main(String[] args) 
	{
		//Given method used for passing values
		//Given method used for passing value
		RestAssured
		.given()
		
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
		.body("{\r\n"
				+ "    \"firstname\" : \"Jim\",\r\n"
				+ "    \"lastname\" : \"Brown\",\r\n"
				+ "    \"totalprice\" : 111,\r\n"
				+ "    \"depositpaid\" : true,\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2018-01-01\",\r\n"
				+ "        \"checkout\" : \"2019-01-01\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
				+ "}")
		//RestAssuredObject.contentType("application/json");
		.contentType("application/json")
		
		//Hit Request and get response
		//When method used for HTTP methods
		.when()
		.post()
		//Validation Response
		//Used for validating response
		.then()
		.statusCode(200)
		.log()
		.all();
		}

}
