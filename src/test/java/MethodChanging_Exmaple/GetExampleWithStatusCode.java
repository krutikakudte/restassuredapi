package MethodChanging_Exmaple;

import io.restassured.RestAssured;

public class GetExampleWithStatusCode {

	public static void main(String[] args) 
	{
		RestAssured
		.given()
		.baseUri("https://simple-books-api.glitch.me/")
		.basePath("status")
		.when()
		.get()
		.then()
		.statusCode(200)
		.log()
		.all();

	}

}
