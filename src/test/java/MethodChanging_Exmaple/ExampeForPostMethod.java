package MethodChanging_Exmaple;

import io.restassured.RestAssured;

public class ExampeForPostMethod 
{
	public static void main(String[] args) 
	{
		RestAssured
		.given()
		.baseUri("https://bookstore.toolsqa.com/Account/v1/")
		.basePath("User")
		.body("{\r\n"
				+ "  \"userName\": \"Kruitka.kudte1237\",\r\n"
				+ "  \"password\": \"Shashank.Honnalli@123\"\r\n"
				+ "}")
		.contentType("application/json")
		.when()
		.post()
		.then()
		.log()
		.all();
//		.then()
//		.statusCode(201)
		
	}
}
