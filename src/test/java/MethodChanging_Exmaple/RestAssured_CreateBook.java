package MethodChanging_Exmaple;

import io.restassured.RestAssured;
//import io.restassured.response.Response;
//import io.restassured.response.ValidatableResponse;
//import io.restassured.specification.RequestSpecification;

public class RestAssured_CreateBook {

	public static void main(String[] args) 
	{
		
		RestAssured
		.given()
		
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
		.body("{\r\n"
				+ "    \"firstname\" : \"Krutika\",\r\n"
				+ "    \"lastname\" : \"Sachin\",\r\n"
				+ "    \"totalprice\" : 121,\r\n"
				+ "    \"depositpaid\" : false,\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2018-01-01\",\r\n"
				+ "        \"checkout\" : \"2019-01-01\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
				+ "}")
		//RestAssuredObject.contentType("application/json");
		.contentType("application/json")
		
		//Hit Request and get response
		
		.post()
		//Validation Response
		.then()
		.log()
		.headers()
		.statusCode(200)
		.log()
		.status()
		.log()
		.body();
	}

}
