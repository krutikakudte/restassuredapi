package MethodChanging_Exmaple;


import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;

public class PassingPathParameter_ByusingMap {

	public static void main(String[] args) 
	{
		Map<String,Object> PathParametre = new HashMap<String,Object>();
		PathParametre.put("BasePath","booking");
		PathParametre.put("Booking_id",15);
		
		RestAssured
			.given()
			.log()
			.all()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("{BasePath}/{Booking_id}")
			.pathParams(PathParametre)
		.when()
			.get()
		.then()
			//.statusCode(200)
			.log()
			.all();
		
		
		// TODO Auto-generated method stub

	}

}
