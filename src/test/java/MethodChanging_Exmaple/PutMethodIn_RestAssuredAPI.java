package MethodChanging_Exmaple;

import io.restassured.RestAssured;

public class PutMethodIn_RestAssuredAPI {

	public static void main(String[] args) 
	{

		RestAssured
			.given()
			.log()
			.all()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("booking/{id}")
			.pathParam("id", 15)
			.header("Content-Type","application/json")
			.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
			.body("{\r\n"
					+ "    \"firstname\" : \"Gururaj\",\r\n"
					+ "    \"lastname\" : \"Honnalli\",\r\n"
					+ "    \"totalprice\" : 111,\r\n"
					+ "    \"depositpaid\" : true,\r\n"
					+ "    \"bookingdates\" : {\r\n"
					+ "        \"checkin\" : \"2018-01-01\",\r\n"
					+ "        \"checkout\" : \"2019-01-01\"\r\n"
					+ "    },\r\n"
					+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
					+ "}")
		.when()
			.put()
		.then()
			.assertThat()
			.statusCode(200)
			.log()
			.all();
		

	}

}
