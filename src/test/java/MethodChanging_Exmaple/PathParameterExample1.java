package MethodChanging_Exmaple;

import io.restassured.RestAssured;

public class PathParameterExample1 {

	public static void main(String[] args) 
	{
		RestAssured
		.given()
			.log()
			.all()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("{BasePath}/{Booking_Id}")
			.pathParam("BasePath", "Booking")
			.pathParam("Booking_Id", 20)
		.when()
			.get()
		.then()
			.log()
			.all();
		
	
	}

}
