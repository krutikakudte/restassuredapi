package MethodChanging_Exmaple;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class ParticularUpdate_Book {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		RestAssured
			.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/")
				.basePath("booking/1")
				//.pathParam("BookId", 1)
				.body("{\r\n"
						+ "    \"firstname\" : \"Krutika\",\r\n"
						+ "    \"lastname\" : \"Kudte\"\r\n"
						+ "}")
				.contentType(ContentType.JSON)
				.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
			.when()
				.patch()
			.then()
				.log()
				.all()
				.assertThat()
				.statusCode(200);
		}

}
