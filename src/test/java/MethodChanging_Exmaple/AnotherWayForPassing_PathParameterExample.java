package MethodChanging_Exmaple;

import io.restassured.RestAssured;

public class AnotherWayForPassing_PathParameterExample {

	public static void main(String[] args) 
	{
		RestAssured
		.given()
			.log()
			.all()
			.baseUri("https://restful-booker.herokuapp.com/")
			//.pathParam("BasePath", "Booking")
			
		.when()
			//.get("https://restful-booker.herokuapp.com/{BasePath}/{Booking_Id}",20)
			.get("https://restful-booker.herokuapp.com/{BasePath}/{Booking_Id}","Booking",28)
		.then()
			.log()
			.all();
		
	
	}

}
